module CollectMinHitPolicy where

import Playground.Contract
import Plutus.Contract
import PlutusCore.Default qualified as PLC
import PlutusTx
import PlutusTx.Lift
import PlutusTx.Builtins
import PlutusTx.Prelude
import Data.Maybe
import Prelude qualified as Haskell (Show, show, String)
import Data.ByteString.Char8 qualified as C


data Decision_pwrj53Output = Decision_pwrj53Output
    { firstMinOutput :: Integer
    , secondMinOutput :: Integer
    }
    deriving stock (Haskell.Show, Generic)
    deriving anyclass (FromJSON, ToJSON, ToSchema, ToArgument)

makeLift ''Decision_pwrj53Output

decision_1pwrj53Rule1 :: Integer -> Haskell.String -> Maybe Decision_pwrj53Output
decision_1pwrj53Rule1 _ _ = Just Decision_pwrj53Output
    { firstMinOutput = 3
    , secondMinOutput = 4
    }

decision_1pwrj53Rule2 :: Integer -> Haskell.String -> Maybe Decision_pwrj53Output
decision_1pwrj53Rule2 numberInput _
    | numberInput < 10 = Just Decision_pwrj53Output
        { firstMinOutput = 1
        , secondMinOutput = 3
        }
    | otherwise = Nothing

decision_1pwrj53Rule3 :: Integer -> Haskell.String -> Maybe Decision_pwrj53Output
decision_1pwrj53Rule3 _ textInput
    | (toBuiltin $ C.pack textInput) == "Matched Test String" = Just Decision_pwrj53Output
        { firstMinOutput = 2
        , secondMinOutput = 5
        }
    | otherwise = Nothing

decision_1pwrj53ApplyRules :: Integer -> Haskell.String -> [Maybe Decision_pwrj53Output]
decision_1pwrj53ApplyRules numberInput textInput = map (\f -> f numberInput textInput) [decision_1pwrj53Rule1, decision_1pwrj53Rule2, decision_1pwrj53Rule3]

decision_1pwrj53Minimum :: Maybe Decision_pwrj53Output -> Maybe  Decision_pwrj53Output -> Maybe Decision_pwrj53Output
decision_1pwrj53Minimum Nothing (Just a) = Just a
decision_1pwrj53Minimum (Just a) (Just b) = Just Decision_pwrj53Output
    { firstMinOutput = (firstMinOutput a `min` firstMinOutput b)
    , secondMinOutput = (secondMinOutput a `min` secondMinOutput b)
    }

decision_1pwrj53Get :: Integer -> Haskell.String -> Maybe Decision_pwrj53Output
decision_1pwrj53Get numberInput textInput
    | (length $ catMaybes $ decision_1pwrj53ApplyRules numberInput textInput) /= 0 
        = foldl decision_1pwrj53Minimum Nothing $ map Just $ catMaybes $ decision_1pwrj53ApplyRules numberInput textInput
    | otherwise = Nothing

-- 'mkSchemaDefinitions' doesn't work with 'EmptySchema'
-- (that is, with 0 endpoints) so we define a
-- dummy schema type with 1 endpoint to make it compile.
-- TODO: Repair 'mkSchemaDefinitions'
data SampleParams = SampleParams
    { numberInput :: Integer
    , textInput :: Haskell.String
    }
    deriving stock (Haskell.Show, Generic)
    deriving anyclass (FromJSON, ToJSON, ToSchema, ToArgument)

decision_1pwrj53 :: AsContractError e => Promise () SampleSchema e ()
decision_1pwrj53 = endpoint @"decision_1pwrj53" @SampleParams $ \(SampleParams ni ti) -> do
    let pn = decision_1pwrj53Get ni ti
    logInfo @Haskell.String $ Haskell.show pn

type SampleSchema =
        Endpoint "decision_1pwrj53" SampleParams

endpoints :: AsContractError e => Contract () SampleSchema e ()
endpoints = selectList [decision_1pwrj53]

mkSchemaDefinitions ''SampleSchema

$(mkKnownCurrencies [])