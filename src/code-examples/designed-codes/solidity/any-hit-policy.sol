// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

contract AnyHitPolicy {

    struct Decision_pwrj53Output {
        uint firstNumberOutput;
        uint secondNumberOutput;
    }

    function decision_1pwrj53(uint numberInput, string memory textInput) public pure returns(Decision_pwrj53Output memory) {
        Decision_pwrj53Output memory output;
        bool matchedRule = false;

        //First rule
        if (!matchedRule) {
            output = Decision_pwrj53Output(3, 4);
            matchedRule = true;
        } else if (output.firstNumberOutput != 3 || output.secondNumberOutput != 4) {
            revert('Undefined output');
        }  
        //Second rule
        if (keccak256(abi.encodePacked(textInput)) == keccak256(abi.encodePacked("Matched Test String"))) {
            if (!matchedRule) {
                output = Decision_pwrj53Output(1, 3);
                matchedRule = true;
            } else if (output.firstNumberOutput != 1 || output.secondNumberOutput != 3) {
                revert('Undefined output');
            }         
        }
        //Third rule
        if (numberInput < 10) {
            if (!matchedRule) {
                output = Decision_pwrj53Output(2, 5);
                matchedRule = true;
            } else if (output.firstNumberOutput != 2 || output.secondNumberOutput != 5) {
                revert('Undefined output');
            }   
        }

        if (!matchedRule) {
            revert('Undefined output');
        }
        return output;
    }
}