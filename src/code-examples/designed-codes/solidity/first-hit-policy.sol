// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

contract FirstHitPolicy {
    
    struct Decision_pwrj53Output {
        uint firstNumberOutput;
        uint secondNumberOutput;
    }

    function decision_1pwrj53(uint numberInput, string memory textInput) public pure returns(Decision_pwrj53Output memory) {
        Decision_pwrj53Output memory output;

        //First rule
        output = Decision_pwrj53Output(3, 4);
        return output;
        //Second rule
        if (keccak256(abi.encodePacked(textInput)) == keccak256(abi.encodePacked("Matched Test String"))) {
            output = Decision_pwrj53Output(1, 3);
            return output;  
        }
        //Third rule
        if (numberInput < 10) {
            output = Decision_pwrj53Output(2, 5);
            return output;  
        }
        
        revert('Undefined output');
    }
}