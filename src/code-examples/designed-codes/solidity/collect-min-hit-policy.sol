// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

contract CollectMinHitPolicy {

    struct Decision_pwrj53Output {
        uint firstMinOutput;
        uint secondMinOutput;
    }

    function decision_1pwrj53(uint numberInput, string memory textInput) public pure returns(Decision_pwrj53Output memory) {
        Decision_pwrj53Output memory output;
        bool matchedRule = false;

        //First rule
        if (!matchedRule) {
            output.firstMinOutput = 3;
            output.secondMinOutput = 4;
        } else {
            if (output.firstMinOutput > 3) {
                output.firstMinOutput = 3;
            }
            if (output.secondMinOutput > 4) {
                output.secondMinOutput = 4;   
            }
        }
        matchedRule = true;

        //Second rule
        if (keccak256(abi.encodePacked(textInput)) == keccak256(abi.encodePacked("Matched Test String"))) {
            if (!matchedRule) {
                output.firstMinOutput = 1;
                output.secondMinOutput = 3;
            } else {
                if (output.firstMinOutput > 1) {
                    output.firstMinOutput = 1;
                }
                if (output.secondMinOutput > 3) {
                    output.secondMinOutput = 3;   
                }
            }
            matchedRule = true;
        }
        //Third rule
        if (numberInput < 10) {
            if (!matchedRule) {
                output.firstMinOutput = 2;
                output.secondMinOutput = 5;
            } else {
                if (output.firstMinOutput > 2) {
                    output.firstMinOutput = 2;
                }
                if (output.secondMinOutput > 5) {
                    output.secondMinOutput = 5;   
                }
            }
            matchedRule = true;
        }

        if (!matchedRule) {
            revert('Undefined output');
        }
        return output;
    }
}