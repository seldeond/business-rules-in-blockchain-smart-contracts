// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

contract CollectCountHitPolicy {

    function decision_1pwrj53(int numberInput, string memory textInput) public pure returns(int) {
        int count = 0;

        //First rule
        count++;

        //Second rule
        if (keccak256(abi.encodePacked(textInput)) == keccak256(abi.encodePacked("Matched Test String"))) {
            count++;
        }
        //Third rule
        if (numberInput < 10) {
            count++;
        }

        return count;
    }
}