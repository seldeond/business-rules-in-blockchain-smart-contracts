// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

contract RuleOrderHitPolicy {

    struct Decision_pwrj53Output {
        uint firstNumberOutput;
        uint secondNumberOutput;
    }

    function decision_1pwrj53(uint numberInput, string memory textInput) public pure returns(Decision_pwrj53Output[] memory) {
        Decision_pwrj53Output[3] memory matches;
        uint outputSize = 0;
        bool matchedRule = false;

        //First rule
        matches[outputSize] = Decision_pwrj53Output(3, 4);
        outputSize++;
        matchedRule = true;

        //Second rule
        if (keccak256(abi.encodePacked(textInput)) == keccak256(abi.encodePacked("Matched Test String"))) {
            matches[outputSize] = Decision_pwrj53Output(1, 3);
            outputSize++;
            matchedRule = true;
        }
        //Third rule
        if (numberInput < 10) {
            matches[outputSize] = Decision_pwrj53Output(2, 5);
            outputSize++;
            matchedRule = true;
        }

        Decision_pwrj53Output[] memory output = new Decision_pwrj53Output[](outputSize);
        for (uint i = 0; i < outputSize; i++) {
            output[i] = matches[i];
        }

        if (!matchedRule) {
            revert('Undefined output');
        }
        return output;
    }
}