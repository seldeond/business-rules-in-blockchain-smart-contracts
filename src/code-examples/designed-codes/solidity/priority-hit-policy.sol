// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

contract PriorityHitPolicy {

    struct Decision_pwrj53Output {
        uint firstNumberOutput;
        uint secondNumberOutput;
    }

    function decision_1pwrj53_decideByPriority(Decision_pwrj53Output[3] memory priorities, Decision_pwrj53Output memory currentOutput, Decision_pwrj53Output memory newOutput) private pure returns(Decision_pwrj53Output memory) {
        for (uint i = 0; i < 3; i++) {
            if (priorities[i].firstNumberOutput == currentOutput.firstNumberOutput
            && priorities[i].secondNumberOutput == currentOutput.secondNumberOutput) {
                return currentOutput;
            } else if (priorities[i].firstNumberOutput == newOutput.firstNumberOutput
            && priorities[i].secondNumberOutput == newOutput.secondNumberOutput) {
                return newOutput;
            }
        }
        revert('Undefined output');
    }

    function decision_1pwrj53(uint numberInput, string memory textInput) public pure returns(Decision_pwrj53Output memory) {
        Decision_pwrj53Output[3] memory priorities = [Decision_pwrj53Output(1, 3), Decision_pwrj53Output(3, 4), Decision_pwrj53Output(2, 5)];
        Decision_pwrj53Output memory output;
        bool matchedRule = false;

        //First rule
        if (!matchedRule) {
            output = Decision_pwrj53Output(3, 4);
            matchedRule = true;
        } else {
            output = decision_1pwrj53_decideByPriority(priorities, output, Decision_pwrj53Output(3, 4));
        }
        //Second rule
        if (keccak256(abi.encodePacked(textInput)) == keccak256(abi.encodePacked("Matched Test String"))) {
            if (!matchedRule) {
                output = Decision_pwrj53Output(1, 3);
                matchedRule = true;
            } else {
                output = decision_1pwrj53_decideByPriority(priorities, output, Decision_pwrj53Output(1, 3));
            }    
        }
        //Third rule
        if (numberInput < 10) {
            if (!matchedRule) {
                output = Decision_pwrj53Output(2, 5);
                matchedRule = true;
            } else {
                output = decision_1pwrj53_decideByPriority(priorities, output, Decision_pwrj53Output(2, 5));
            }    
        }

        if (!matchedRule) {
            revert('Undefined output');
        }
        return output;
    }
}