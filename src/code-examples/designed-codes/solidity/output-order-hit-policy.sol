// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

contract OutputOrderHitPolicy {

    struct Decision_pwrj53Output {
        uint firstNumberOutput;
        uint secondNumberOutput;
    }

    function decision_1pwrj53(uint numberInput, string memory textInput) public pure returns(Decision_pwrj53Output[] memory) {
        Decision_pwrj53Output[3] memory priorities = [Decision_pwrj53Output(1, 3), Decision_pwrj53Output(3, 4), Decision_pwrj53Output(2, 5)];
        bool[3] memory existsInOutput;
        uint outputSize = 0;
        bool matchedRule = false;

        //First rule
        for (uint i = 0; i < 3; i++) {
            if (!existsInOutput[i] 
            && priorities[i].firstNumberOutput == 3
            && priorities[i].secondNumberOutput == 4) {
                existsInOutput[i] = true;
                outputSize++;
                matchedRule = true;
                break;
            }
        }
        //Second rule
        if (keccak256(abi.encodePacked(textInput)) == keccak256(abi.encodePacked("Matched Test String"))) {
            for (uint i = 0; i < 3; i++) {
                if (!existsInOutput[i] 
                && priorities[i].firstNumberOutput == 1
                && priorities[i].secondNumberOutput == 3) {
                    existsInOutput[i] = true;
                    outputSize++;
                    matchedRule = true;
                    break;
                }
            }
        }
        //Third rule
        if (numberInput < 10) {
            for (uint i = 0; i < 3; i++) {
                if (!existsInOutput[i] 
                && priorities[i].firstNumberOutput == 2
                && priorities[i].secondNumberOutput == 5) {
                    existsInOutput[i] = true;
                    outputSize++;
                    matchedRule = true;
                    break;
                }
            }
        }

        uint j = 0;
        Decision_pwrj53Output[] memory output = new Decision_pwrj53Output[](outputSize);
        for (uint i = 0; i < 3; i++) {
            if (existsInOutput[i]) {
                output[j] = Decision_pwrj53Output(priorities[i].firstNumberOutput, priorities[i].secondNumberOutput);
                j++;
            }
        }

        if (!matchedRule) {
            revert('Undefined output');
        }
        return output;
    }
}