// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

contract CollectMaxHitPolicy {

    struct Decision_pwrj53Output {
        uint firstMaxOutput;
        uint secondMaxOutput;
    }

    function decision_1pwrj53(uint numberInput, string memory textInput) public pure returns(Decision_pwrj53Output memory) {
        Decision_pwrj53Output memory output;
        bool matchedRule = false;

        //First rule
        if (!matchedRule) {
            output.firstMaxOutput = 3;
            output.secondMaxOutput = 4;
        } else {
            if (output.firstMaxOutput < 3) {
                output.firstMaxOutput = 3;
            }
            if (output.secondMaxOutput < 4) {
                output.secondMaxOutput = 4;   
            }
        }
        matchedRule = true;

        //Second rule
        if (keccak256(abi.encodePacked(textInput)) == keccak256(abi.encodePacked("Matched Test String"))) {
            if (!matchedRule) {
                output.firstMaxOutput = 1;
                output.secondMaxOutput = 3;
            } else {
                if (output.firstMaxOutput < 1) {
                    output.firstMaxOutput = 1;
                }
                if (output.secondMaxOutput < 3) {
                    output.secondMaxOutput = 3;   
                }
            }
            matchedRule = true;
        }
        //Third rule
        if (numberInput < 10) {
            if (!matchedRule) {
                output.firstMaxOutput = 2;
                output.secondMaxOutput = 5;
            } else {
                if (output.firstMaxOutput < 2) {
                    output.firstMaxOutput = 2;
                }
                if (output.secondMaxOutput < 5) {
                    output.secondMaxOutput = 5;   
                }
            }
            matchedRule = true;
        }

        if (!matchedRule) {
            revert('Undefined output');
        }
        return output;
    }
}