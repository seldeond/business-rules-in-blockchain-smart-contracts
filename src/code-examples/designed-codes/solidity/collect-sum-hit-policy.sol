// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

contract CollectSumHitPolicy {

    struct Decision_pwrj53Output {
        uint firstSumOutput;
        uint secondSumOutput;
    }

    function decision_1pwrj53(uint numberInput, string memory textInput) public pure returns(Decision_pwrj53Output memory) {
        Decision_pwrj53Output memory output;
        bool matchedRule = false;
        output.firstSumOutput = 0;
        output.secondSumOutput = 0;

        //First rule
        output.firstSumOutput += 3;
        output.secondSumOutput += 4;
        matchedRule = true;

        //Second rule
        if (keccak256(abi.encodePacked(textInput)) == keccak256(abi.encodePacked("Matched Test String"))) {
            output.firstSumOutput += 1;
            output.secondSumOutput += 3;
            matchedRule = true;
        }
        //Third rule
        if (numberInput < 10) {
            output.firstSumOutput += 2;
            output.secondSumOutput += 5;
            matchedRule = true;
        }

        if (!matchedRule) {
            revert('Undefined output');
        }
        return output;
    }
}