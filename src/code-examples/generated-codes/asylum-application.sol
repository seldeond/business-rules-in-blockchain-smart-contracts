pragma solidity ^0.8.7;





contract contract0a21cf776b174a6390ec815214dce662 { 
	

	mapping(string => bool) Process_0dqe426_Activity_TakeBackActiveStates;

	

	mapping(string => bool) Process_1vtzte3_Activity_FinalDecisionTakingActiveStates;

	

	mapping(string => bool) Process_1bglz33_Activity_ApplicationValidationActiveStates;

	

	mapping(string => bool) Process_172rzzq_Activity_TakeChargeActiveStates;

	

	mapping(string => bool) Process_1x95f9r_Activity_ApplicationPriorizationActiveStates;

	

	mapping(string => bool) Process_1ActiveStates;

	Application application;

	struct Application{
		uint256 id;
		string status;
		string finalDecision;
		SubsidiaryInterview subsidiaryInterview;
		TakeBackRequest takeBackRequest;
		TakeChargeRequest takeChargeRequest;
		VoluntaryReturnProposition voluntaryReturnProposition;
		Appeal admissibilityAppeal;
		uint openedAtDate;
		ApplicantInfo applicant;
		bool pendingSubProtectionRecom;
		bool pendingRATAppeal;
		bool pendingRefStatusRecom;
		string ministerialUnitDecision;
		bool wellFounded;
		int streamOnePriority;
		int streamTwoPriority;
		Appeal finalDecisionAppeal;
		uint256 temporaryRegistrationCertificateId;
		string threatsAtCountryOfOrigin;
	}

	struct VoluntaryReturnProposition{
		bool isAccepted;
	}

	struct SubsidiaryInterview{
		bool wasPerformed;
		uint256 transcriptId;
	}

	struct TakeBackRequest{
		bool isValid;
		Appeal appeal;
		bool isApproved;
		string justification;
		string requestedCountry;
	}

	struct TakeChargeRequest{
		bool isValid;
		Appeal appeal;
		bool isApproved;
		string justification;
		string requestedCountry;
	}

	struct Appeal{
		bool isUpheld;
		string appealDecision;
	}

	struct ApplicantInfo{
		int believedAgeInInterview;
		bool familyGroup;
		bool unaccompaniedMinor;
		string countryOfOrigin;
		bool threateningMedicalCond;
		bool existingInternationalProtection;
		bool nonRefoulmentPrinciple;
	}

	function isStateActiveProcess_1(string memory state) public view returns(bool){
		return Process_1ActiveStates[state];
	}

	constructor() public payable{
		Process_1ActiveStates["Activity_ApplicationValidation"] = true;
		Activity_ApplicationValidation();
	}

	function Activity_ApplicationPriorization() internal {
		Process_1x95f9r_Activity_ApplicationPriorizationActiveStates["Activity_ApplicationPriorization_Activity_InternationalProtectionQuestionnaire"] = true;
	}

	function Activity_ApplicationPriorizationReturnLogic() internal {
		Process_1ActiveStates["Activity_ApplicationPriorization"] = false;
		Process_1ActiveStates["Activity_FinalDecisionTaking"] = true;
		Activity_FinalDecisionTaking();
	}

	function Takechargerequest() internal {
		Process_172rzzq_Activity_TakeChargeActiveStates["Activity_TakeCharge_Activity_TakeChargeRequest"] = true;
	}

	function TakechargerequestReturnLogic() internal {
		Process_1ActiveStates["Takechargerequest"] = false;
		Gateway_0j2crp4();
	}

	function Activity_ApplicationValidation() internal {
		Process_1bglz33_Activity_ApplicationValidationActiveStates["Activity_ApplicationValidation_Activity_PreliminaryInfo"] = true;
	}

	function Activity_ApplicationValidationReturnLogic() internal {
		Process_1ActiveStates["Activity_ApplicationValidation"] = false;
		Gateway_05dh9vg();
	}

	function Activity_FinalDecisionTaking() internal {
		Process_1vtzte3_Activity_FinalDecisionTakingActiveStates["Activity_FinalDecisionTaking_Activity_SubsidiaryInfo"] = true;
	}

	function Activity_FinalDecisionTakingReturnLogic() internal {
		Process_1ActiveStates["Activity_FinalDecisionTaking"] = false;
		Gateway_1qr1pwb();
	}

	function Gateway_05dh9vg() internal {
		if(keccak256(abi.encodePacked(application.status)) != keccak256(abi.encodePacked("Rejected"))){
			Process_1ActiveStates["Activity_1th7mjv"] = true;
		}
		else if(keccak256(abi.encodePacked(application.status)) == keccak256(abi.encodePacked("Rejected"))){
			Process_1ActiveStates["Applicationrejected"] = true;
		}
	}

	function Gateway_1qr1pwb() internal {
		if(application.subsidiaryInterview.wasPerformed == true){
			Finaldecision();
		}
		else if(application.subsidiaryInterview.wasPerformed == false){
			Gateway_1kiszla();
		}
	}

	function Gateway_0r7ck5t() internal {
		if(application.takeBackRequest.isValid == true){
			Process_1ActiveStates["Takebackrequest"] = true;
			Takebackrequest();
		}
		else if(application.takeBackRequest.isValid == false){
			Gateway_05zmnx1();
		}
	}

	function Gateway_05zmnx1() internal {
		Process_1ActiveStates["Activity_ReviewTakeCharge"] = true;
	}

	function Finaldecision() internal {
		if(keccak256(abi.encodePacked(application.finalDecision)) == keccak256(abi.encodePacked("RefugeeStatus"))){
			Process_1ActiveStates["Event_00v933a"] = true;
		}
		else if(keccak256(abi.encodePacked(application.finalDecision)) == keccak256(abi.encodePacked("SubsidiaryProtection"))){
			Process_1ActiveStates["Event_008rlnh"] = true;
		}
		else if(keccak256(abi.encodePacked(application.finalDecision)) == keccak256(abi.encodePacked("PermissionToRemain"))){
			Process_1ActiveStates["Event_00988y7"] = true;
		}
		else if(keccak256(abi.encodePacked(application.finalDecision)) == keccak256(abi.encodePacked("VoluntaryReturn"))){
			Process_1ActiveStates["Activity_CheckVoluntaryReturn"] = true;
		}
		else if(keccak256(abi.encodePacked(application.finalDecision)) == keccak256(abi.encodePacked("DeportationOrder"))){
			Gateway_1kiszla();
		}
	}

	function Gateway_1vzjld9() internal {
		if(application.takeChargeRequest.isValid == true){
			Process_1ActiveStates["Takechargerequest"] = true;
			Takechargerequest();
		}
		else if(application.takeChargeRequest.isValid == false){
			Gateway_0bkn8vj();
		}
	}

	function Takebackrequest() internal {
		Process_0dqe426_Activity_TakeBackActiveStates["Activity_TakeBack_Activity_TakeBackRequest"] = true;
	}

	function TakebackrequestReturnLogic() internal {
		Process_1ActiveStates["Takebackrequest"] = false;
		Gateway_1pyb5n1();
	}

	modifier isActivity_1th7mjvState{
		require(isStateActiveProcess_1("Activity_1th7mjv") == true);
		_;
	}

	function Activity_1th7mjv(bool takeBackRequestValid) isActivity_1th7mjvState() public {
		application.takeBackRequest.isValid = takeBackRequestValid;
		Process_1ActiveStates["Activity_1th7mjv"] = false;
		Gateway_0r7ck5t();
	}

	modifier isActivity_ReviewTakeChargeState{
		require(isStateActiveProcess_1("Activity_ReviewTakeCharge") == true);
		_;
	}

	function Activity_ReviewTakeCharge(bool takeChargeRequestValid) isActivity_ReviewTakeChargeState() public {
		application.takeChargeRequest.isValid = takeChargeRequestValid;
		Process_1ActiveStates["Activity_ReviewTakeCharge"] = false;
		Gateway_1vzjld9();
	}

	function Gateway_1pyb5n1() internal {
		if(keccak256(abi.encodePacked(application.status)) != keccak256(abi.encodePacked("Overhanded"))){
			Gateway_05zmnx1();
		}
		else if(keccak256(abi.encodePacked(application.status)) == keccak256(abi.encodePacked("Overhanded"))){
			Process_1ActiveStates["Event_13ubgqt"] = true;
		}
	}

	function Gateway_0bkn8vj() internal {
		Process_1ActiveStates["Activity_ApplicationPriorization"] = true;
		Activity_ApplicationPriorization();
	}

	function Gateway_0j2crp4() internal {
		if(keccak256(abi.encodePacked(application.status)) != keccak256(abi.encodePacked("Overhanded"))){
			Gateway_0bkn8vj();
		}
		else if(keccak256(abi.encodePacked(application.status)) == keccak256(abi.encodePacked("Overhanded"))){
			Process_1ActiveStates["Event_0o7bbfv"] = true;
		}
	}

	function Gateway_0f2xcdk() internal {
		if(application.voluntaryReturnProposition.isAccepted == true){
			Process_1ActiveStates["Event_1wrf61v"] = true;
		}
		else if(application.voluntaryReturnProposition.isAccepted == false){
			Gateway_0j8jklk();
		}
	}

	function Gateway_0j8jklk() internal {
		Process_1ActiveStates["Event_1d1vgjb"] = true;
	}

	modifier isActivity_CheckVoluntaryReturnState{
		require(isStateActiveProcess_1("Activity_CheckVoluntaryReturn") == true);
		_;
	}

	function Activity_CheckVoluntaryReturn(bool voluntaryReturnAccepted) isActivity_CheckVoluntaryReturnState() public {
		application.voluntaryReturnProposition.isAccepted = voluntaryReturnAccepted;
		Process_1ActiveStates["Activity_CheckVoluntaryReturn"] = false;
		Gateway_0f2xcdk();
	}

	function Gateway_1kiszla() internal {
		Gateway_0j8jklk();
	}

	function isStateActiveProcess_1x95f9r_Activity_ApplicationPriorization(string memory state) public view returns(bool){
		return Process_1x95f9r_Activity_ApplicationPriorizationActiveStates[state];
	}

	function Activity_ApplicationPriorization_Gateway_1lc9hx3() internal {
		if(application.openedAtDate < 20160111000000){
			Activity_ApplicationPriorization_Activity_StreamTwo();
		}
		else if(application.openedAtDate >= 20160111000000){
			Activity_ApplicationPriorization_Activity_StreamOne();
		}
	}

	struct Decision_1kp1qf8Output{
		int application__streamTwoPriority;
	}

	function decision_1kp1qf8() internal view returns(Decision_1kp1qf8Output memory){
		Decision_1kp1qf8Output memory output;
		if(application.applicant.believedAgeInInterview > 70 && application.applicant.familyGroup == false){
			output = Decision_1kp1qf8Output(1);
			return output;
		}
		if(application.applicant.unaccompaniedMinor == true){
			output = Decision_1kp1qf8Output(1);
			return output;
		}
		if(application.wellFounded == true){
			output = Decision_1kp1qf8Output(2);
			return output;
		}
		if(keccak256(abi.encodePacked(application.applicant.countryOfOrigin)) == keccak256(abi.encodePacked("Syria"))){
			output = Decision_1kp1qf8Output(3);
			return output;
		}
		if(keccak256(abi.encodePacked(application.applicant.countryOfOrigin)) == keccak256(abi.encodePacked("Eritrea"))){
			output = Decision_1kp1qf8Output(3);
			return output;
		}
		if(keccak256(abi.encodePacked(application.applicant.countryOfOrigin)) == keccak256(abi.encodePacked("Iraq"))){
			output = Decision_1kp1qf8Output(3);
			return output;
		}
		if(keccak256(abi.encodePacked(application.applicant.countryOfOrigin)) == keccak256(abi.encodePacked("Afghanistan"))){
			output = Decision_1kp1qf8Output(3);
			return output;
		}
		if(keccak256(abi.encodePacked(application.applicant.countryOfOrigin)) == keccak256(abi.encodePacked("Iran"))){
			output = Decision_1kp1qf8Output(3);
			return output;
		}
		if(keccak256(abi.encodePacked(application.applicant.countryOfOrigin)) == keccak256(abi.encodePacked("Libya"))){
			output = Decision_1kp1qf8Output(3);
			return output;
		}
		if(keccak256(abi.encodePacked(application.applicant.countryOfOrigin)) == keccak256(abi.encodePacked("Somalia"))){
			output = Decision_1kp1qf8Output(3);
			return output;
		}
		if(application.applicant.threateningMedicalCond == true){
			output = Decision_1kp1qf8Output(4);
			return output;
		}
		revert('Undefined output');
	}

	function Activity_ApplicationPriorization_Activity_StreamTwo() internal {
		Decision_1kp1qf8Output memory decision_1kp1qf8Output = decision_1kp1qf8();
		application.streamTwoPriority = decision_1kp1qf8Output.application__streamTwoPriority;
		Activity_ApplicationPriorization_Gateway_0povfn5();
	}

	function Activity_ApplicationPriorization_Gateway_0povfn5() internal {
		Process_1x95f9r_Activity_ApplicationPriorizationActiveStates["Activity_ApplicationPriorization_Prioritycalculated"] = true;
		Activity_ApplicationPriorizationReturnLogic();
	}

	struct Decision_52ugzjqOutput{
		int application__streamOnePriority;
	}

	function decision_52ugzjq() internal view returns(Decision_52ugzjqOutput memory){
		Decision_52ugzjqOutput memory output;
		if(application.pendingSubProtectionRecom == true){
			output = Decision_52ugzjqOutput(1);
			return output;
		}
		if(application.pendingRATAppeal == true){
			output = Decision_52ugzjqOutput(2);
			return output;
		}
		if(application.pendingRefStatusRecom == true){
			output = Decision_52ugzjqOutput(3);
			return output;
		}
		revert('Undefined output');
	}

	function Activity_ApplicationPriorization_Activity_StreamOne() internal {
		Decision_52ugzjqOutput memory decision_52ugzjqOutput = decision_52ugzjq();
		application.streamOnePriority = decision_52ugzjqOutput.application__streamOnePriority;
		Activity_ApplicationPriorization_Gateway_0povfn5();
	}

	modifier isActivity_ApplicationPriorization_Activity_InternationalProtectionQuestionnaireState{
		require(isStateActiveProcess_1x95f9r_Activity_ApplicationPriorization("Activity_ApplicationPriorization_Activity_InternationalProtectionQuestionnaire") == true);
		_;
	}

	function Activity_ApplicationPriorization_Activity_InternationalProtectionQuestionnaire(bool hasThreateningMedicalCondition, bool subsidiaryProtectionRecommendation, bool pendingRefugeeAppealTribunalAppeal, bool refugeeStatusRecommendation, string memory threateningConditionsAtCountryOfOrigin) isActivity_ApplicationPriorization_Activity_InternationalProtectionQuestionnaireState() public {
		application.pendingSubProtectionRecom = subsidiaryProtectionRecommendation;
		application.pendingRATAppeal = pendingRefugeeAppealTribunalAppeal;
		application.pendingRefStatusRecom = refugeeStatusRecommendation;
		application.applicant.threateningMedicalCond = hasThreateningMedicalCondition;
		application.threatsAtCountryOfOrigin = threateningConditionsAtCountryOfOrigin;
		Process_1x95f9r_Activity_ApplicationPriorizationActiveStates["Activity_ApplicationPriorization_Activity_InternationalProtectionQuestionnaire"] = false;
		Activity_ApplicationPriorization_Gateway_1lc9hx3();
	}

	function isStateActiveProcess_172rzzq_Activity_TakeCharge(string memory state) public view returns(bool){
		return Process_172rzzq_Activity_TakeChargeActiveStates[state];
	}

	function Activity_TakeCharge_Gateway_1bfhxg3() internal {
		if(application.takeChargeRequest.isApproved == false){
			Process_172rzzq_Activity_TakeChargeActiveStates["Activity_TakeCharge_Event_1ybnxft"] = true;
			TakechargerequestReturnLogic();
		}
		else if(application.takeChargeRequest.isApproved == true){
			Process_172rzzq_Activity_TakeChargeActiveStates["Activity_TakeCharge_Activity_TakeChargeAppeal"] = true;
		}
	}

	modifier isActivity_TakeCharge_Activity_ApproveTakeChargeState{
		require(isStateActiveProcess_172rzzq_Activity_TakeCharge("Activity_TakeCharge_Activity_ApproveTakeCharge") == true);
		_;
	}

	function Activity_TakeCharge_Activity_ApproveTakeCharge(bool requestApproval) isActivity_TakeCharge_Activity_ApproveTakeChargeState() public {
		application.takeChargeRequest.isApproved = requestApproval;
		Process_172rzzq_Activity_TakeChargeActiveStates["Activity_TakeCharge_Activity_ApproveTakeCharge"] = false;
		Activity_TakeCharge_Gateway_1bfhxg3();
	}

	function Activity_TakeCharge_Wasappealupheld() internal {
		if(application.takeChargeRequest.appeal.isUpheld == false){
			Process_172rzzq_Activity_TakeChargeActiveStates["Activity_TakeCharge_Event_1wuay5h"] = true;
			TakechargerequestReturnLogic();
		}
		else if(application.takeChargeRequest.appeal.isUpheld == true){
			Process_172rzzq_Activity_TakeChargeActiveStates["Activity_TakeCharge_Event_1l0wnij"] = true;
			TakechargerequestReturnLogic();
		}
	}

	modifier isActivity_TakeCharge_Activity_TakeChargeAppealState{
		require(isStateActiveProcess_172rzzq_Activity_TakeCharge("Activity_TakeCharge_Activity_TakeChargeAppeal") == true);
		_;
	}

	function Activity_TakeCharge_Activity_TakeChargeAppeal(bool appealRaised, bool appealUpheld) isActivity_TakeCharge_Activity_TakeChargeAppealState() public {
		application.takeChargeRequest.appeal.appealDecision = "Overhanded";
		application.status = "Overhanded";
		application.takeChargeRequest.appeal.isUpheld = false;
		if (appealRaised == true) {
			if (appealUpheld == true) {
		    	application.status = "Admissible";
				application.takeChargeRequest.appeal.appealDecision = "Admissible";
				application.takeChargeRequest.appeal.isUpheld = true;
			}
		}
		Process_172rzzq_Activity_TakeChargeActiveStates["Activity_TakeCharge_Activity_TakeChargeAppeal"] = false;
		Activity_TakeCharge_Wasappealupheld();
	}

	modifier isActivity_TakeCharge_Activity_TakeChargeRequestState{
		require(isStateActiveProcess_172rzzq_Activity_TakeCharge("Activity_TakeCharge_Activity_TakeChargeRequest") == true);
		_;
	}

	function Activity_TakeCharge_Activity_TakeChargeRequest(string memory requestedDublinCountry, string memory justificationForTheRequest) isActivity_TakeCharge_Activity_TakeChargeRequestState() public {
		application.takeChargeRequest.requestedCountry = requestedDublinCountry;
		application.takeChargeRequest.justification = justificationForTheRequest;
		Process_172rzzq_Activity_TakeChargeActiveStates["Activity_TakeCharge_Activity_TakeChargeRequest"] = false;
		Process_172rzzq_Activity_TakeChargeActiveStates["Activity_TakeCharge_Activity_ApproveTakeCharge"] = true;
	}

	function isStateActiveProcess_1bglz33_Activity_ApplicationValidation(string memory state) public view returns(bool){
		return Process_1bglz33_Activity_ApplicationValidationActiveStates[state];
	}

	modifier isActivity_ApplicationValidation_Activity_PreliminaryInfoState{
		require(isStateActiveProcess_1bglz33_Activity_ApplicationValidation("Activity_ApplicationValidation_Activity_PreliminaryInfo") == true);
		_;
	}

	function Activity_ApplicationValidation_Activity_PreliminaryInfo(uint believedAge, bool partOfFamilyGroup, bool isUnaccompaniedMinor, string memory applicantsCountryOfOrigin, bool internationalProtectionAlreadyExists, bool hasNonRefoulmentPrinciple, uint applicationOpenedAtDate) isActivity_ApplicationValidation_Activity_PreliminaryInfoState() public {
		application.openedAtDate = applicationOpenedAtDate;
		
		application.applicant.believedAgeInInterview = int(believedAge);
		application.applicant.familyGroup = partOfFamilyGroup;
		application.applicant.unaccompaniedMinor = isUnaccompaniedMinor;
		application.applicant.countryOfOrigin = applicantsCountryOfOrigin;
		application.applicant.existingInternationalProtection = internationalProtectionAlreadyExists;
		application.applicant.nonRefoulmentPrinciple = hasNonRefoulmentPrinciple;
		
		application.status = "Opened";
		Process_1bglz33_Activity_ApplicationValidationActiveStates["Activity_ApplicationValidation_Activity_PreliminaryInfo"] = false;
		Activity_ApplicationValidation_Activity_ReviewAdmissibility();
	}

	function Activity_ApplicationValidation_Gateway_051r8ve() internal {
		if(keccak256(abi.encodePacked(application.status)) == keccak256(abi.encodePacked("Admissible"))){
			Activity_ApplicationValidation_Gateway_0rt4v1p();
		}
		else if(keccak256(abi.encodePacked(application.status)) == keccak256(abi.encodePacked("Inadmissible"))){
			Process_1bglz33_Activity_ApplicationValidationActiveStates["Activity_ApplicationValidation_Activity_AdmissibilityAppeal"] = true;
		}
	}

	struct Decision_i7tm6k0Output{
		string application__status;
	}

	function decision_i7tm6k0() internal view returns(Decision_i7tm6k0Output memory){
		Decision_i7tm6k0Output memory output;
		if(keccak256(abi.encodePacked(application.applicant.countryOfOrigin)) == keccak256(abi.encodePacked("Bosnia and Herzegovina"))){
			output = Decision_i7tm6k0Output("Inadmissible");
			return output;
		}
		if(keccak256(abi.encodePacked(application.applicant.countryOfOrigin)) == keccak256(abi.encodePacked("North Macedonia"))){
			output = Decision_i7tm6k0Output("Inadmissible");
			return output;
		}
		if(keccak256(abi.encodePacked(application.applicant.countryOfOrigin)) == keccak256(abi.encodePacked("Georgie"))){
			output = Decision_i7tm6k0Output("Inadmissible");
			return output;
		}
		if(keccak256(abi.encodePacked(application.applicant.countryOfOrigin)) == keccak256(abi.encodePacked("Kosovo"))){
			output = Decision_i7tm6k0Output("Inadmissible");
			return output;
		}
		if(keccak256(abi.encodePacked(application.applicant.countryOfOrigin)) == keccak256(abi.encodePacked("Montenegro"))){
			output = Decision_i7tm6k0Output("Inadmissible");
			return output;
		}
		if(keccak256(abi.encodePacked(application.applicant.countryOfOrigin)) == keccak256(abi.encodePacked("Republic of Albania"))){
			output = Decision_i7tm6k0Output("Inadmissible");
			return output;
		}
		if(keccak256(abi.encodePacked(application.applicant.countryOfOrigin)) == keccak256(abi.encodePacked("Republic of Serbia"))){
			output = Decision_i7tm6k0Output("Inadmissible");
			return output;
		}
		if(keccak256(abi.encodePacked(application.applicant.countryOfOrigin)) == keccak256(abi.encodePacked("Republic of South Africa"))){
			output = Decision_i7tm6k0Output("Inadmissible");
			return output;
		}
		if(application.applicant.existingInternationalProtection == true && application.applicant.nonRefoulmentPrinciple == true){
			output = Decision_i7tm6k0Output("Inadmissible");
			return output;
		}
		output = Decision_i7tm6k0Output("Admissible");
		return output;
	}

	function Activity_ApplicationValidation_Activity_ReviewAdmissibility() internal {
		Decision_i7tm6k0Output memory decision_i7tm6k0Output = decision_i7tm6k0();
		application.status = decision_i7tm6k0Output.application__status;
		Process_1bglz33_Activity_ApplicationValidationActiveStates["Activity_ApplicationValidation_Activity_ApproveAdmissibility"] = true;
	}

	modifier isActivity_ApplicationValidation_Activity_ApproveAdmissibilityState{
		require(isStateActiveProcess_1bglz33_Activity_ApplicationValidation("Activity_ApplicationValidation_Activity_ApproveAdmissibility") == true);
		_;
	}

	function Activity_ApplicationValidation_Activity_ApproveAdmissibility(bool wellFoundedApplication, bool approveDecision) isActivity_ApplicationValidation_Activity_ApproveAdmissibilityState() public {
		application.id = uint(keccak256(abi.encodePacked(block.timestamp,block.difficulty, msg.sender)));
		application.wellFounded = wellFoundedApplication;
		if (approveDecision == true) {
			if (keccak256(abi.encodePacked(application.finalDecisionAppeal.appealDecision)) == keccak256(abi.encodePacked("Admissible"))) {
		    	application.status = "Admissible";
			} else {
		    	application.status = "Inadmissible";
			}
		} else {
			if (keccak256(abi.encodePacked(application.finalDecisionAppeal.appealDecision)) == keccak256(abi.encodePacked("Admissible"))) {
		    	application.status = "Inadmissible";
			} else {
		    	application.status = "Admissible";
			}
		}
		Process_1bglz33_Activity_ApplicationValidationActiveStates["Activity_ApplicationValidation_Activity_ApproveAdmissibility"] = false;
		Activity_ApplicationValidation_Gateway_051r8ve();
	}

	function Activity_ApplicationValidation_Gateway_0rt4v1p() internal {
		Activity_ApplicationValidation_Activity_TemCert();
	}

	function Activity_ApplicationValidation_Wasappealupheld() internal {
		if(application.admissibilityAppeal.isUpheld == true){
			Activity_ApplicationValidation_Gateway_0rt4v1p();
		}
		else if(application.admissibilityAppeal.isUpheld == false){
			Process_1bglz33_Activity_ApplicationValidationActiveStates["Activity_ApplicationValidation_Applicationrejected"] = true;
			Activity_ApplicationValidationReturnLogic();
		}
	}

	modifier isActivity_ApplicationValidation_Activity_AdmissibilityAppealState{
		require(isStateActiveProcess_1bglz33_Activity_ApplicationValidation("Activity_ApplicationValidation_Activity_AdmissibilityAppeal") == true);
		_;
	}

	function Activity_ApplicationValidation_Activity_AdmissibilityAppeal(bool appealRaised, bool appealUpheld) isActivity_ApplicationValidation_Activity_AdmissibilityAppealState() public {
		application.admissibilityAppeal.appealDecision = "Inadmissible";
		application.status = "Inadmissible";
		application.admissibilityAppeal.isUpheld = false;
		if (appealRaised == true) {
			if (appealUpheld == true) {
		    	application.status = "Admissible";
				application.admissibilityAppeal.appealDecision = "Admissible";
				application.admissibilityAppeal.isUpheld = true;
			}
		}
		Process_1bglz33_Activity_ApplicationValidationActiveStates["Activity_ApplicationValidation_Activity_AdmissibilityAppeal"] = false;
		Activity_ApplicationValidation_Wasappealupheld();
	}

	function Activity_ApplicationValidation_Activity_TemCert() internal {
		application.temporaryRegistrationCertificateId = uint(keccak256(abi.encodePacked(block.timestamp,block.difficulty, msg.sender)));
		Process_1bglz33_Activity_ApplicationValidationActiveStates["Activity_ApplicationValidation_Event_1c34gml"] = true;
		Activity_ApplicationValidationReturnLogic();
	}

	function isStateActiveProcess_1vtzte3_Activity_FinalDecisionTaking(string memory state) public view returns(bool){
		return Process_1vtzte3_Activity_FinalDecisionTakingActiveStates[state];
	}

	function Activity_FinalDecisionTaking_Gateway_1jpt3gt() internal {
		if(application.subsidiaryInterview.wasPerformed == false){
			Process_1vtzte3_Activity_FinalDecisionTakingActiveStates["Activity_FinalDecisionTaking_Event_0w0u5uf"] = true;
			Activity_FinalDecisionTakingReturnLogic();
		}
		else if(application.subsidiaryInterview.wasPerformed == true){
			Process_1vtzte3_Activity_FinalDecisionTakingActiveStates["Activity_FinalDecisionTaking_Activity_FirstInstanceReport"] = true;
		}
	}

	modifier isActivity_FinalDecisionTaking_Activity_FirstInstanceReportState{
		require(isStateActiveProcess_1vtzte3_Activity_FinalDecisionTaking("Activity_FinalDecisionTaking_Activity_FirstInstanceReport") == true);
		_;
	}

	function Activity_FinalDecisionTaking_Activity_FirstInstanceReport(string memory firstInstanceDecision) isActivity_FinalDecisionTaking_Activity_FirstInstanceReportState() public {
		application.ministerialUnitDecision = firstInstanceDecision;
		Process_1vtzte3_Activity_FinalDecisionTakingActiveStates["Activity_FinalDecisionTaking_Activity_FirstInstanceReport"] = false;
		Process_1vtzte3_Activity_FinalDecisionTakingActiveStates["Activity_FinalDecisionTaking_Activity_FirstInstanceAppeal"] = true;
	}

	modifier isActivity_FinalDecisionTaking_Activity_FirstInstanceAppealState{
		require(isStateActiveProcess_1vtzte3_Activity_FinalDecisionTaking("Activity_FinalDecisionTaking_Activity_FirstInstanceAppeal") == true);
		_;
	}

	function Activity_FinalDecisionTaking_Activity_FirstInstanceAppeal(bool appealRaised, bool appealUpheld, string memory decisionOfTheAppeal) isActivity_FinalDecisionTaking_Activity_FirstInstanceAppealState() public {
		application.admissibilityAppeal.isUpheld = false;
		if (appealRaised == true) {
			if (appealUpheld == true) {
				application.admissibilityAppeal.appealDecision = decisionOfTheAppeal;
				application.admissibilityAppeal.isUpheld = true;
			}
		}
		Process_1vtzte3_Activity_FinalDecisionTakingActiveStates["Activity_FinalDecisionTaking_Activity_FirstInstanceAppeal"] = false;
		Activity_FinalDecisionTaking_Makefinaldecision();
	}

	struct Decision_89l3lakOutput{
		string application__finalDecision;
	}

	function decision_89l3lak() internal view returns(Decision_89l3lakOutput memory){
		Decision_89l3lakOutput memory output;
		if(keccak256(abi.encodePacked(application.finalDecisionAppeal.appealDecision)) == keccak256(abi.encodePacked("Refugee status"))){
			output = Decision_89l3lakOutput("Refugee status");
			return output;
		}
		if(keccak256(abi.encodePacked(application.finalDecisionAppeal.appealDecision)) == keccak256(abi.encodePacked("Subsidiary protection"))){
			output = Decision_89l3lakOutput("Subsidiary protection");
			return output;
		}
		if(keccak256(abi.encodePacked(application.finalDecisionAppeal.appealDecision)) == keccak256(abi.encodePacked("Permission to remain"))){
			output = Decision_89l3lakOutput("Permission to remain");
			return output;
		}
		if(keccak256(abi.encodePacked(application.ministerialUnitDecision)) == keccak256(abi.encodePacked("Refugee status"))){
			output = Decision_89l3lakOutput("Refugee status");
			return output;
		}
		if(keccak256(abi.encodePacked(application.ministerialUnitDecision)) == keccak256(abi.encodePacked("Subsidiary protection"))){
			output = Decision_89l3lakOutput("Subsidiary protection");
			return output;
		}
		if(keccak256(abi.encodePacked(application.ministerialUnitDecision)) == keccak256(abi.encodePacked("Permission to remain"))){
			output = Decision_89l3lakOutput("Permission to remain");
			return output;
		}
		if(keccak256(abi.encodePacked(application.ministerialUnitDecision)) == keccak256(abi.encodePacked("Voluntary return proposition"))){
			output = Decision_89l3lakOutput("Voluntary return proposition");
			return output;
		}
		if(keccak256(abi.encodePacked(application.ministerialUnitDecision)) == keccak256(abi.encodePacked("Deportation order"))){
			output = Decision_89l3lakOutput("Deportation order");
			return output;
		}
		revert('Undefined output');
	}

	function Activity_FinalDecisionTaking_Makefinaldecision() internal {
		Decision_89l3lakOutput memory decision_89l3lakOutput = decision_89l3lak();
		application.finalDecision = decision_89l3lakOutput.application__finalDecision;
		Process_1vtzte3_Activity_FinalDecisionTakingActiveStates["Activity_FinalDecisionTaking_Activity_FinalDecisionApproval"] = true;
	}

	modifier isActivity_FinalDecisionTaking_Activity_FinalDecisionApprovalState{
		require(isStateActiveProcess_1vtzte3_Activity_FinalDecisionTaking("Activity_FinalDecisionTaking_Activity_FinalDecisionApproval") == true);
		_;
	}

	function Activity_FinalDecisionTaking_Activity_FinalDecisionApproval(bool signFinalDecision) isActivity_FinalDecisionTaking_Activity_FinalDecisionApprovalState() public {
		if (signFinalDecision == true || signFinalDecision == false) {
		    application.status = application.finalDecision;
		}
		Process_1vtzte3_Activity_FinalDecisionTakingActiveStates["Activity_FinalDecisionTaking_Activity_FinalDecisionApproval"] = false;
		Process_1vtzte3_Activity_FinalDecisionTakingActiveStates["Activity_FinalDecisionTaking_Event_02pp2mo"] = true;
		Activity_FinalDecisionTakingReturnLogic();
	}

	modifier isActivity_FinalDecisionTaking_Activity_SubsidiaryInfoState{
		require(isStateActiveProcess_1vtzte3_Activity_FinalDecisionTaking("Activity_FinalDecisionTaking_Activity_SubsidiaryInfo") == true);
		_;
	}

	function Activity_FinalDecisionTaking_Activity_SubsidiaryInfo(bool wasInterviewPerformed, uint transcriptOfInterviewId) isActivity_FinalDecisionTaking_Activity_SubsidiaryInfoState() public {
		application.subsidiaryInterview.wasPerformed = wasInterviewPerformed;
		application.subsidiaryInterview.transcriptId = transcriptOfInterviewId;
		Process_1vtzte3_Activity_FinalDecisionTakingActiveStates["Activity_FinalDecisionTaking_Activity_SubsidiaryInfo"] = false;
		Activity_FinalDecisionTaking_Gateway_1jpt3gt();
	}

	function isStateActiveProcess_0dqe426_Activity_TakeBack(string memory state) public view returns(bool){
		return Process_0dqe426_Activity_TakeBackActiveStates[state];
	}

	function Activity_TakeBack_Gateway_12tjk18() internal {
		if(application.takeBackRequest.isApproved == false){
			Process_0dqe426_Activity_TakeBackActiveStates["Activity_TakeBack_Event_0mbnp4x"] = true;
			TakebackrequestReturnLogic();
		}
		else if(application.takeBackRequest.isApproved == true){
			Process_0dqe426_Activity_TakeBackActiveStates["Activity_TakeBack_Activity_TakeBackAppeal"] = true;
		}
	}

	function Activity_TakeBack_Wasappealupheld() internal {
		if(application.takeBackRequest.appeal.isUpheld == false){
			Process_0dqe426_Activity_TakeBackActiveStates["Activity_TakeBack_Event_1sce1x7"] = true;
			TakebackrequestReturnLogic();
		}
		else if(application.takeBackRequest.appeal.isUpheld == true){
			Process_0dqe426_Activity_TakeBackActiveStates["Activity_TakeBack_Event_0gkn41i"] = true;
			TakebackrequestReturnLogic();
		}
	}

	modifier isActivity_TakeBack_Activity_TakeBackAppealState{
		require(isStateActiveProcess_0dqe426_Activity_TakeBack("Activity_TakeBack_Activity_TakeBackAppeal") == true);
		_;
	}

	function Activity_TakeBack_Activity_TakeBackAppeal(bool appealRaised, bool appealUpheld) isActivity_TakeBack_Activity_TakeBackAppealState() public {
		application.takeBackRequest.appeal.appealDecision = "Overhanded";
		application.status = "Overhanded";
		application.takeBackRequest.appeal.isUpheld = false;
		if (appealRaised == true) {
			if (appealUpheld == true) {
		    	application.status = "Admissible";
				application.takeBackRequest.appeal.appealDecision = "Admissible";
				application.takeBackRequest.appeal.isUpheld = true;
			}
		}
		Process_0dqe426_Activity_TakeBackActiveStates["Activity_TakeBack_Activity_TakeBackAppeal"] = false;
		Activity_TakeBack_Wasappealupheld();
	}

	modifier isActivity_TakeBack_Activity_ApproveTakeBackState{
		require(isStateActiveProcess_0dqe426_Activity_TakeBack("Activity_TakeBack_Activity_ApproveTakeBack") == true);
		_;
	}

	function Activity_TakeBack_Activity_ApproveTakeBack(bool requestApproval) isActivity_TakeBack_Activity_ApproveTakeBackState() public {
		application.takeBackRequest.isApproved = requestApproval;
		Process_0dqe426_Activity_TakeBackActiveStates["Activity_TakeBack_Activity_ApproveTakeBack"] = false;
		Activity_TakeBack_Gateway_12tjk18();
	}

	modifier isActivity_TakeBack_Activity_TakeBackRequestState{
		require(isStateActiveProcess_0dqe426_Activity_TakeBack("Activity_TakeBack_Activity_TakeBackRequest") == true);
		_;
	}

	function Activity_TakeBack_Activity_TakeBackRequest(string memory requestedDublinCountry, string memory justificationForTheRequest) isActivity_TakeBack_Activity_TakeBackRequestState() public {
		application.takeBackRequest.requestedCountry = requestedDublinCountry;
		application.takeBackRequest.justification = justificationForTheRequest;
		Process_0dqe426_Activity_TakeBackActiveStates["Activity_TakeBack_Activity_TakeBackRequest"] = false;
		Process_0dqe426_Activity_TakeBackActiveStates["Activity_TakeBack_Activity_ApproveTakeBack"] = true;
	}

 }