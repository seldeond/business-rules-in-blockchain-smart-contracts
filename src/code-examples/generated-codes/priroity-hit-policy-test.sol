pragma solidity ^0.8.7;





contract contract8a01571ced4a48caae70982f291b5e66 { 
	

	mapping(string => bool) Process_1ActiveStates;

	int categoryTwo;

	int categoryOne;

	Person person;

	struct Person{
		string status;
		string name;
		int age;
		uint256 dateOfBirth;
	}

	function isStateActiveProcess_1(string memory state) public view returns(bool){
		return Process_1ActiveStates[state];
	}

	constructor() public payable{
		Process_1ActiveStates["Fillinformation"] = true;
	}

	struct Decision_kowu89qOutput{
		int categoryOne;
		int categoryTwo;
	}

	function decision_kowu89q_decideByPriority(Decision_kowu89qOutput[4] memory priorities, Decision_kowu89qOutput memory currentOutput, Decision_kowu89qOutput memory newOutput) internal view returns(Decision_kowu89qOutput memory){
		for(uint256 i = 0; i < 4; i++){ 
			if(priorities[i].categoryOne == currentOutput.categoryOne && priorities[i].categoryTwo == currentOutput.categoryTwo){
				return currentOutput;
			}
			else if(priorities[i].categoryOne == newOutput.categoryOne && priorities[i].categoryTwo == newOutput.categoryTwo){
				return newOutput;
			}
		}
		revert('Undefined output');
	}

	function decision_kowu89q() internal view returns(Decision_kowu89qOutput memory){
		Decision_kowu89qOutput[4] memory priorities = [Decision_kowu89qOutput(1, 2), Decision_kowu89qOutput(5, 6), Decision_kowu89qOutput(0, 0), Decision_kowu89qOutput(3, 4)];
		Decision_kowu89qOutput memory output;
		bool matchedRule = false;
		if(person.age == 18 && keccak256(abi.encodePacked(person.name)) == keccak256(abi.encodePacked("Paul"))){
			if(!matchedRule){
				output = Decision_kowu89qOutput(5, 6);
				matchedRule = true;
			}
			else {
				output = decision_kowu89q_decideByPriority(priorities, output, Decision_kowu89qOutput(5, 6));
			}
		}
		if(person.age > 18 && keccak256(abi.encodePacked(person.name)) == keccak256(abi.encodePacked("Peter"))){
			if(!matchedRule){
				output = Decision_kowu89qOutput(3, 4);
				matchedRule = true;
			}
			else {
				output = decision_kowu89q_decideByPriority(priorities, output, Decision_kowu89qOutput(3, 4));
			}
		}
		if(person.dateOfBirth > 20000101){
			if(!matchedRule){
				output = Decision_kowu89qOutput(1, 2);
				matchedRule = true;
			}
			else {
				output = decision_kowu89q_decideByPriority(priorities, output, Decision_kowu89qOutput(1, 2));
			}
		}
		if(!matchedRule){
			output = Decision_kowu89qOutput(0, 0);
			matchedRule = true;
		}
		else {
			output = decision_kowu89q_decideByPriority(priorities, output, Decision_kowu89qOutput(0, 0));
		}
		if(!matchedRule){
			revert('Undefined output');
		}
		return output;
	}

	function Assignoutput() internal {
		Decision_kowu89qOutput memory decision_kowu89qOutput = decision_kowu89q();
		categoryOne = decision_kowu89qOutput.categoryOne;
		categoryTwo = decision_kowu89qOutput.categoryTwo;
		Process_1ActiveStates["Endevent"] = true;
	}

	modifier isFillinformationState{
		require(isStateActiveProcess_1("Fillinformation") == true);
		_;
	}

	function Fillinformation(string memory personName, uint personAge, uint birthday) isFillinformationState() public {
		person.age = int(personAge);
		person.name = personName;
		person.dateOfBirth = birthday;
		Process_1ActiveStates["Fillinformation"] = false;
		Assignoutput();
	}

 }