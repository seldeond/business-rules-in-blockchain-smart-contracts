pragma solidity ^0.8.7;





contract contract8a01571ced4a48caae70982f291b5e66 { 
	

	mapping(string => bool) Process_1ActiveStates;

	int categoryTwo;

	int categoryOne;

	Person person;

	struct Person{
		string status;
		string name;
		int age;
		uint256 dateOfBirth;
	}

	function isStateActiveProcess_1(string memory state) public view returns(bool){
		return Process_1ActiveStates[state];
	}

	constructor() public payable{
		Process_1ActiveStates["Fillinformation"] = true;
	}

	struct Decision_kowu89qOutput{
		int categoryOne;
		int categoryTwo;
	}

	function decision_kowu89q() internal view returns(Decision_kowu89qOutput memory){
		Decision_kowu89qOutput memory output;
		bool matchedRule = false;
		if(person.age == 18 && keccak256(abi.encodePacked(person.name)) == keccak256(abi.encodePacked("Paul"))){
			if(!matchedRule){
				output.categoryOne = 5;
				output.categoryTwo = 6;
			}
			else {
				if(output.categoryOne < 5){
					output.categoryOne = 5;
				}
				if(output.categoryTwo < 6){
					output.categoryTwo = 6;
				}
			}
			matchedRule = true;
		}
		if(person.age > 18 && keccak256(abi.encodePacked(person.name)) == keccak256(abi.encodePacked("Peter"))){
			if(!matchedRule){
				output.categoryOne = 3;
				output.categoryTwo = 4;
			}
			else {
				if(output.categoryOne < 3){
					output.categoryOne = 3;
				}
				if(output.categoryTwo < 4){
					output.categoryTwo = 4;
				}
			}
			matchedRule = true;
		}
		if(person.dateOfBirth > 20000101){
			if(!matchedRule){
				output.categoryOne = 1;
				output.categoryTwo = 2;
			}
			else {
				if(output.categoryOne < 1){
					output.categoryOne = 1;
				}
				if(output.categoryTwo < 2){
					output.categoryTwo = 2;
				}
			}
			matchedRule = true;
		}
		if(!matchedRule){
			output.categoryOne = 0;
			output.categoryTwo = 0;
		}
		else {
			if(output.categoryOne < 0){
				output.categoryOne = 0;
			}
			if(output.categoryTwo < 0){
				output.categoryTwo = 0;
			}
		}
		matchedRule = true;
		if(!matchedRule){
			revert('Undefined output');
		}
		return output;
	}

	function Assignoutput() internal {
		Decision_kowu89qOutput memory decision_kowu89qOutput = decision_kowu89q();
		categoryOne = decision_kowu89qOutput.categoryOne;
		categoryTwo = decision_kowu89qOutput.categoryTwo;
		Process_1ActiveStates["Endevent"] = true;
	}

	modifier isFillinformationState{
		require(isStateActiveProcess_1("Fillinformation") == true);
		_;
	}

	function Fillinformation(string memory personName, uint personAge, uint birthday) isFillinformationState() public {
		person.age = int(personAge);
		person.name = personName;
		person.dateOfBirth = birthday;
		Process_1ActiveStates["Fillinformation"] = false;
		Assignoutput();
	}

 }