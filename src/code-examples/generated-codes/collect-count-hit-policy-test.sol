pragma solidity ^0.8.7;





contract contract8a01571ced4a48caae70982f291b5e66 { 
	

	mapping(string => bool) Process_1ActiveStates;

	int categoryTwo;

	int categoryOne;

	Person person;

	struct Person{
		string status;
		string name;
		int age;
		uint256 dateOfBirth;
	}

	function isStateActiveProcess_1(string memory state) public view returns(bool){
		return Process_1ActiveStates[state];
	}

	constructor() public payable{
		Process_1ActiveStates["Fillinformation"] = true;
	}

	struct Decision_kowu89qOutput{
		int categoryOne;
	}

	function decision_kowu89q() internal view returns(Decision_kowu89qOutput memory){
		Decision_kowu89qOutput memory output;
		output.categoryOne = 0;
		if(person.age == 18 && keccak256(abi.encodePacked(person.name)) == keccak256(abi.encodePacked("Paul"))){
			output.categoryOne++;
		}
		if(person.age > 18 && keccak256(abi.encodePacked(person.name)) == keccak256(abi.encodePacked("Peter"))){
			output.categoryOne++;
		}
		if(person.dateOfBirth > 20000101){
			output.categoryOne++;
		}
		output.categoryOne++;
		return output;
	}

	function Assignoutput() internal {
		Decision_kowu89qOutput memory decision_kowu89qOutput = decision_kowu89q();
		categoryOne = decision_kowu89qOutput.categoryOne;
		Process_1ActiveStates["Endevent"] = true;
	}

	modifier isFillinformationState{
		require(isStateActiveProcess_1("Fillinformation") == true);
		_;
	}

	function Fillinformation(string memory personName, uint personAge, uint birthday) isFillinformationState() public {
		person.age = int(personAge);
		person.name = personName;
		person.dateOfBirth = birthday;
		Process_1ActiveStates["Fillinformation"] = false;
		Assignoutput();
	}

 }