pragma solidity ^0.8.7;





contract contract8a01571ced4a48caae70982f291b5e66 { 
	

	mapping(string => bool) Process_1ActiveStates;

	int categoryTwo;

	int categoryOne;

	Person person;

	struct Person{
		string status;
		string name;
		int age;
		uint256 dateOfBirth;
	}

	function isStateActiveProcess_1(string memory state) public view returns(bool){
		return Process_1ActiveStates[state];
	}

	constructor() public payable{
		Process_1ActiveStates["Fillinformation"] = true;
	}

	struct Decision_kowu89qOutput{
		int categoryOne;
		int categoryTwo;
	}

	function decision_kowu89q() internal view returns(Decision_kowu89qOutput memory){
		Decision_kowu89qOutput memory output;
		if(person.age == 18 && keccak256(abi.encodePacked(person.name)) == keccak256(abi.encodePacked("Paul"))){
			output = Decision_kowu89qOutput(5, 6);
			return output;
		}
		if(person.age > 18 && keccak256(abi.encodePacked(person.name)) == keccak256(abi.encodePacked("Peter"))){
			output = Decision_kowu89qOutput(3, 4);
			return output;
		}
		if(person.dateOfBirth > 20000101){
			output = Decision_kowu89qOutput(1, 2);
			return output;
		}
		output = Decision_kowu89qOutput(0, 0);
		return output;
	}

	struct Decision_0gbt2s0Output{
		int person__age;
	}

	function decision_0gbt2s0() internal view returns(Decision_0gbt2s0Output memory){
		Decision_0gbt2s0Output memory output;
		if(keccak256(abi.encodePacked(person.status)) == keccak256(abi.encodePacked("child"))){
			output = Decision_0gbt2s0Output(10);
			return output;
		}
		if(keccak256(abi.encodePacked(person.status)) == keccak256(abi.encodePacked("adult"))){
			output = Decision_0gbt2s0Output(20);
			return output;
		}
		output = Decision_0gbt2s0Output(18);
		return output;
	}

	function Assignoutput() internal {
		Decision_0gbt2s0Output memory decision_0gbt2s0Output = decision_0gbt2s0();
		person.age = decision_0gbt2s0Output.person__age;
		Decision_kowu89qOutput memory decision_kowu89qOutput = decision_kowu89q();
		categoryOne = decision_kowu89qOutput.categoryOne;
		categoryTwo = decision_kowu89qOutput.categoryTwo;
		Process_1ActiveStates["Endevent"] = true;
	}

	modifier isFillinformationState{
		require(isStateActiveProcess_1("Fillinformation") == true);
		_;
	}

	function Fillinformation(string memory personName, string memory personStatus, uint birthday) isFillinformationState() public {
		person.status = personStatus;
		person.name = personName;
		person.dateOfBirth = birthday;
		Process_1ActiveStates["Fillinformation"] = false;
		Assignoutput();
	}

 }