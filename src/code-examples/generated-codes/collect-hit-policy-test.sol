pragma solidity ^0.8.7;





contract contract8a01571ced4a48caae70982f291b5e66 { 
	

	mapping(string => bool) Process_1ActiveStates;

	int[] categoryTwo;

	int[] categoryOne;

	Person person;

	struct Person{
		string status;
		string name;
		int age;
		uint256 dateOfBirth;
	}

	function isStateActiveProcess_1(string memory state) public view returns(bool){
		return Process_1ActiveStates[state];
	}

	constructor() public payable{
		Process_1ActiveStates["Fillinformation"] = true;
	}

	struct Decision_kowu89qOutput{
		int categoryOne;
		int categoryTwo;
	}

	function decision_kowu89q() internal view returns(Decision_kowu89qOutput[] memory){
		Decision_kowu89qOutput[4] memory priorities = [Decision_kowu89qOutput(5, 6), Decision_kowu89qOutput(3, 4), Decision_kowu89qOutput(1, 2), Decision_kowu89qOutput(0, 0)];
		bool[4] memory existsInOutput;
		uint outputSize = 0;
		bool matchedRule = false;
		if(person.age == 18 && keccak256(abi.encodePacked(person.name)) == keccak256(abi.encodePacked("Paul"))){
			for(uint256 i = 0; i < 4; i++){ 
				if(!existsInOutput[i] && priorities[i].categoryOne == 5 && priorities[i].categoryTwo == 6){
					existsInOutput[i] = true;
					outputSize++;
					matchedRule = true;
					break;
				}
			}
		}
		if(person.age > 18 && keccak256(abi.encodePacked(person.name)) == keccak256(abi.encodePacked("Peter"))){
			for(uint256 i = 0; i < 4; i++){ 
				if(!existsInOutput[i] && priorities[i].categoryOne == 3 && priorities[i].categoryTwo == 4){
					existsInOutput[i] = true;
					outputSize++;
					matchedRule = true;
					break;
				}
			}
		}
		if(person.dateOfBirth > 20000101){
			for(uint256 i = 0; i < 4; i++){ 
				if(!existsInOutput[i] && priorities[i].categoryOne == 1 && priorities[i].categoryTwo == 2){
					existsInOutput[i] = true;
					outputSize++;
					matchedRule = true;
					break;
				}
			}
		}
		for(uint256 i = 0; i < 4; i++){ 
			if(!existsInOutput[i] && priorities[i].categoryOne == 0 && priorities[i].categoryTwo == 0){
				existsInOutput[i] = true;
				outputSize++;
				matchedRule = true;
				break;
			}
		}
		uint j = 0;
		Decision_kowu89qOutput[] memory output = new Decision_kowu89qOutput[](outputSize);
		for(uint256 i = 0; i < 4; i++){ 
			if(existsInOutput[i]){
				output[j] = Decision_kowu89qOutput(priorities[i].categoryOne, priorities[i].categoryTwo);
				j++;
			}
		}
		if(!matchedRule){
			revert('Undefined output');
		}
		return output;
	}

	function Assignoutput() internal {
		Decision_kowu89qOutput[] memory decision_kowu89qOutput = decision_kowu89q();
		for(uint256 i = 0; i < decision_kowu89qOutput.length; i++){
		  categoryOne.push(decision_kowu89qOutput[i].categoryOne);
		  categoryTwo.push(decision_kowu89qOutput[i].categoryTwo);
		}
		Process_1ActiveStates["Endevent"] = true;
	}

	modifier isFillinformationState{
		require(isStateActiveProcess_1("Fillinformation") == true);
		_;
	}

	function Fillinformation(string memory personName, uint personAge, uint birthday) isFillinformationState() public {
		person.age = int(personAge);
		person.name = personName;
		person.dateOfBirth = birthday;
		Process_1ActiveStates["Fillinformation"] = false;
		Assignoutput();
	}

 }