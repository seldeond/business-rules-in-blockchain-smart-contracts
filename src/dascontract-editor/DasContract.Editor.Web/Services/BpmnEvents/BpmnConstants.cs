﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DasContract.Editor.Web.Services.BpmnEvents
{
    public static class BpmnConstants
    {
        public const string BPMN_ELEMENT_PROCESS = "bpmn:Process";
        public const string BPMN_ELEMENT_PARTICIPANT = "bpmn:Participant";
    }
}
