#pragma checksum "C:\Users\oseld\source\repos\DasContract\DasContract.Editor.Web\Pages\BpmnEditor.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "d4e5223b85dc00d0eec3f5fc72e712ad468eb7ea"
// <auto-generated/>
#pragma warning disable 1591
namespace DasContract.Editor.Web.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Users\oseld\source\repos\DasContract\DasContract.Editor.Web\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\oseld\source\repos\DasContract\DasContract.Editor.Web\_Imports.razor"
using System.Net.Http.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\oseld\source\repos\DasContract\DasContract.Editor.Web\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\oseld\source\repos\DasContract\DasContract.Editor.Web\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\oseld\source\repos\DasContract\DasContract.Editor.Web\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\oseld\source\repos\DasContract\DasContract.Editor.Web\_Imports.razor"
using Microsoft.AspNetCore.Components.Web.Virtualization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\oseld\source\repos\DasContract\DasContract.Editor.Web\_Imports.razor"
using Microsoft.AspNetCore.Components.WebAssembly.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\oseld\source\repos\DasContract\DasContract.Editor.Web\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\oseld\source\repos\DasContract\DasContract.Editor.Web\_Imports.razor"
using DasContract.Editor.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Users\oseld\source\repos\DasContract\DasContract.Editor.Web\_Imports.razor"
using DasContract.Editor.Web.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\Users\oseld\source\repos\DasContract\DasContract.Editor.Web\_Imports.razor"
using DasContract.Abstraction;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "C:\Users\oseld\source\repos\DasContract\DasContract.Editor.Web\_Imports.razor"
using DasContract.Editor.Web.Components.ProcessDetail.GeneralTabs;

#line default
#line hidden
#nullable disable
#nullable restore
#line 13 "C:\Users\oseld\source\repos\DasContract\DasContract.Editor.Web\_Imports.razor"
using DasContract.Editor.Web.Components.ProcessDetail;

#line default
#line hidden
#nullable disable
#nullable restore
#line 14 "C:\Users\oseld\source\repos\DasContract\DasContract.Editor.Web\_Imports.razor"
using DasContract.Editor.Web.Components.Buttons;

#line default
#line hidden
#nullable disable
#nullable restore
#line 15 "C:\Users\oseld\source\repos\DasContract\DasContract.Editor.Web\_Imports.razor"
using DasContract.Editor.Web.Services.ContractManagement;

#line default
#line hidden
#nullable disable
#nullable restore
#line 16 "C:\Users\oseld\source\repos\DasContract\DasContract.Editor.Web\_Imports.razor"
using DasContract.Editor.Web.Components.Select2;

#line default
#line hidden
#nullable disable
#nullable restore
#line 17 "C:\Users\oseld\source\repos\DasContract\DasContract.Editor.Web\_Imports.razor"
using BlazorMonaco;

#line default
#line hidden
#nullable disable
#nullable restore
#line 18 "C:\Users\oseld\source\repos\DasContract\DasContract.Editor.Web\_Imports.razor"
using BlazorPro.BlazorSize;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/process")]
    public partial class BpmnEditor : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.OpenElement(0, "div");
            __builder.AddAttribute(1, "class", "editor-main");
            __builder.AddAttribute(2, "b-t7a7tuvwet");
            __builder.OpenElement(3, "div");
            __builder.AddAttribute(4, "class", (
#nullable restore
#line 3 "C:\Users\oseld\source\repos\DasContract\DasContract.Editor.Web\Pages\BpmnEditor.razor"
                  ShowDetailBar ? "grid" : ""

#line default
#line hidden
#nullable disable
            ) + " main-container");
            __builder.AddAttribute(5, "b-t7a7tuvwet");
#nullable restore
#line 4 "C:\Users\oseld\source\repos\DasContract\DasContract.Editor.Web\Pages\BpmnEditor.razor"
         if (UserFormService.IsPreviewOpen)
        {

#line default
#line hidden
#nullable disable
            __builder.OpenElement(6, "div");
            __builder.AddAttribute(7, "class", "user-form-preview");
            __builder.AddAttribute(8, "b-t7a7tuvwet");
            __builder.OpenComponent<DasContract.Editor.Web.Components.UserForms.DForm>(9);
            __builder.AddAttribute(10, "UserForm", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<DasContract.Abstraction.UserInterface.UserForm>(
#nullable restore
#line 7 "C:\Users\oseld\source\repos\DasContract\DasContract.Editor.Web\Pages\BpmnEditor.razor"
                                                                             UserFormService.CurrentUserForm

#line default
#line hidden
#nullable disable
            ));
            __builder.CloseComponent();
            __builder.CloseElement();
#nullable restore
#line 9 "C:\Users\oseld\source\repos\DasContract\DasContract.Editor.Web\Pages\BpmnEditor.razor"
        }
        else
        {

#line default
#line hidden
#nullable disable
            __builder.AddMarkupContent(11, "<div class=\"outer-canvas\" b-t7a7tuvwet><div id=\"canvas\" b-t7a7tuvwet></div></div>");
#nullable restore
#line 15 "C:\Users\oseld\source\repos\DasContract\DasContract.Editor.Web\Pages\BpmnEditor.razor"
        }

#line default
#line hidden
#nullable disable
#nullable restore
#line 16 "C:\Users\oseld\source\repos\DasContract\DasContract.Editor.Web\Pages\BpmnEditor.razor"
         if (ShowDetailBar)
        {

#line default
#line hidden
#nullable disable
            __builder.AddMarkupContent(12, "<div class=\"gutter-col gutter-col-1\" b-t7a7tuvwet></div>\r\n            ");
            __builder.OpenComponent<DasContract.Editor.Web.Components.ProcessDetail.ProcessDetailBar>(13);
            __builder.CloseComponent();
#nullable restore
#line 20 "C:\Users\oseld\source\repos\DasContract\DasContract.Editor.Web\Pages\BpmnEditor.razor"
        }

#line default
#line hidden
#nullable disable
            __builder.CloseElement();
            __builder.CloseElement();
        }
        #pragma warning restore 1998
    }
}
#pragma warning restore 1591
