<div id="top"></div>

# Use the zip file to build the DasContract Editor. The previous uploads to gitlab were incomplete.

## The structure of the enclosed CD

* **images** - contains images for this readme file
* **models** - the thesis model directory
  * **class-diagrams** - class diagrams of the DasContract Editor
    * added and modified classes by this thesis are marked grey
  * **dmn-and-bpmn-models** - models for the asylum procedure
* **src** - the directory of source codes
  * **code-examples** - code examples in Solidity and Plutus
    * **designed-codes** - not-generated codes that were basis for the templates
      * both Solidity and Plutus compilable using Remix IDE and Plutus Playground
      * https://remix.ethereum.org/
      * https://playground.plutus.iohkdev.io/
    * **generated-codes** - generated solidity codes used for manual tests and case study
    * **source-contracts** - contract files for dasContract editor
      * contracts for manual tests and case study
  * **dascontract-editor** - implementation sources of the altered dasContract editor
  * **thesis** - the directory of LATEX source codes of the thesis
* **text** - contains the thesis text in PDF format

## Getting Started

Below you can find instructions to run and test the solution.

### Prerequisites

* Microsoft Visual Studio
* Npm Package Manager

### Installation

1. Open solution src/dascontract-editor/DasContract.sln in Visual Studio
2. Navigate to DasContract/Dascontract.Editor.Web/wwwroot
3. Run `npm install` to install all node dependencies
4. Run `npm run build` to run a build script (packs all js files and copies dependencies into the dist folder)
5. Set DasContract.Editor.Web as the startup project and run it.

### Note to behavior
* Large Solidity smart contracts may result in a Warning that the solution will not work in mainnet. The solution is still compilable, deployable, and possible to test. Proceed with force send.
* The original converter can have some issues and was not the focus of this thesis. Before conversion, it is recommended to save the solution, and load it again. 

<p align="right">(<a href="#top">back to top</a>)</p>

### Simple Tutorial

In this section, you can see a simple tutorial to load, generate and deploy the smart contract from the business rule.

1. Load one of the contracts created for manual tests from the folder src/code-examples/source-contracts/
    * for example FirstHitPolicyTest.dascontract
![Tutorial01][tut01]
2. Click on the business rule task Assign output.
![Tutorial02][tut02]
3. Display the decision table of the business rule through the button Rules.
    * It will be helpful to see it for the testing.
![Tutorial03][tut03]
4. Generate the contract with Convert->Solidity.
![Tutorial04][tut04]
5. Copy the generated Solidity code.
![Tutorial05][tut05]
6. Open the Remix IDE: https://remix.ethereum.org/
7. Create a new folder with a new .sol file and paste the generated code into it.
![Tutorial06][tut06]
8. Saving the file should be sufficient to compile the code, but it is also possible on a separate tab.
![Tutorial07][tut07]
9. Click on the Deploy button and expand the contract and the endpoint.
![Tutorial08][tut08]
10. Fill it with data (date format must be inserted as uint in format yyyymmdd) and click on the Transact button.
11. Click on the debug button next to a successful transaction log.
![Tutorial09][tut09]
12. Use a slider to see how the data evolve according to the input data and the logic of the defined business rule.
![Tutorial10][tut10]

[tut01]: images/tut01.png
[tut02]: images/tut02.png
[tut03]: images/tut03.png
[tut04]: images/tut04.png
[tut05]: images/tut05.png
[tut06]: images/tut06.png
[tut07]: images/tut07.png
[tut08]: images/tut08.png
[tut09]: images/tut09.png
[tut10]: images/tut10.png

<p align="right">(<a href="#top">back to top</a>)</p>